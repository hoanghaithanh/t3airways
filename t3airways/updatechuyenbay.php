<?php 
session_start();
require_once('Connections/Myconnection.php');
if (($_SESSION['logged-in']!=true) or ($_SESSION['user_name']=='admin')) {
  header("Location: Index.php");
}
require_once('admin_hangbay_tool.php');
?>
<?php

$diemroiErr = $diemdenErr = $maybayErr = $soghetoidaErr = $thoigiandiErr = $thoigiandenErr = "";
	$diemroi = $diemden = $cuaden = $cuadi = $maybay = $soghetoida = $thoigiandi = $thoigianden = "";
$searchsuccess = 0;
$validated = 0;
$submit = $_POST['submit'];
if($submit == "Hủy")
{
	header("Location: index.php");
}
if($submit=="Tìm Kiếm")
{

	if(empty($_POST['chuyenbayid']))
	{
		$validated = 0;
		$hanhkhachidErr = "Bạn phải nhập ID chuyến bay cần sửa thông tin";
	}
	else $chuyenbayid = $_POST['chuyenbayid'];
	$sql1 = pg_query("SELECT * FROM chuyenbay WHERE chuyenbayid = '".$chuyenbayid."' AND hangbayid = '".$_SESSION['user_name']."'");
	$confirm = pg_num_rows($sql1);
	if($confirm >= 1)
	{
		$searchsuccess = 1;
		$row_RCdanh_sach = pg_fetch_assoc($sql1);
		$diemroi = $row_RCdanh_sach['diemroi'];
		$diemden = $row_RCdanh_sach['diemden'];
		if($diemroi == "Hanoi"){$chuyenbayden = false;} else $chuyenbayden = true;
		$cuaden = $row_RCdanh_sach['cuaden'];
		$cuadi = $row_RCdanh_sach['cuadi'];
		$maybay = $row_RCdanh_sach['maybay'];
		$soghetoida = $row_RCdanh_sach['soghetoida'];
		$soghedadat = $row_RCdanh_sach['soghedadat'];
		$concho = $row_RCdanh_sach['concho'];
		$thoigianden = $row_RCdanh_sach['thoigianden'];
		$thoigiandi = $row_RCdanh_sach['thoigiandi'];
		$namdi = substr($thoigiandi,0,4);
		$thangdi = substr($thoigiandi,5,2);
		$ngaydi = substr($thoigiandi,8,2);
		$giodi = substr($thoigiandi,11,2);
		$phutdi = substr($thoigiandi,14,2);
		$giaydi = "00";
		$namden = substr($thoigianden,0,4);
		$thangden = substr($thoigianden,5,2);
		$ngayden = substr($thoigianden,8,2);
		$gioden = substr($thoigianden,11,2);
		$phutden = substr($thoigianden,14,2);
		$giayden = "00";
        $time_now = date("Y-m-d H:i:s");
        if ($time_now > $thoigiandi)
        {
          $searchsuccess = 0;
          $chuyenbayidErr = "Chuyến bay này đã cất cánh";
        }
	}
	else $chuyenbayidErr = "Không tồn tại ID đã nhập hoặc chuyến bay không thuộc quản lý của hãng bay!";
}
if($submit == "Update")
{	
	$chuyenbayid = $_POST['chuyenbayid'];
	$searchsuccess = 1;
	$validated = 1;
  $cuadi = $_POST['cuadi'];
  $cuaden = $_POST['cuaden'];
	$diemroi = $_POST['diemroi'];
	$diemden = $_POST['diemden'];
	if(empty($_POST['maybay']))
	{
		$validated = 0;
		$hotenErr = "Bạn chưa điền loại máy bay!";
	}
	else $maybay = $maybayupdate = $_POST['maybay'];

	if(empty($_POST['soghetoida']))
	{
		$validated = 0;
		$diachiErr = "Bạn chưa điền số ghế tối đa!";
	}
	else $soghetoida = $soghetoidaupdate = $_POST['soghetoida'];
	$soghedadat = $_POST['soghedadat'];
	
	if(empty($_POST['namdi']) or empty($_POST['ngaydi']) or empty($_POST['thangdi']) or empty($_POST['giodi']) or empty($_POST['phutdi']))
	{
		$validated = 0;
		$thoigiandiErr = "Bạn chưa điền đủ thời gian đi!";
	}
	else 
	{
		$namdi = $_POST['namdi'];
		$thangdi = $_POST['thangdi'];
		$ngaydi = $_POST['ngaydi'];
		$giodi = $_POST['giodi'];
		$phutdi = $_POST['phutdi'];
		if((checkdate($_POST['thangdi'], $_POST['ngaydi'], $_POST['namdi'])==FALSE) or ($giodi>24) or ($giodi<0) or ($phutdi>59) or ($phutdi<0))
		{
			$validated = 0;
			$thoigiandiErr = "Thời gian đi đã nhập không hợp lệ";
		}

	}
	if(empty($_POST['namden']) or empty($_POST['ngayden']) or empty($_POST['thangden']) or empty($_POST['gioden']) or empty($_POST['phutden']))
	{
		$validated = 0;
		$thoigiandenErr = "Bạn chưa điền đủ thời gian đến!";
	}
	else 
	{
		$namden = $_POST['namden'];
		$thangden = $_POST['thangden'];
		$ngayden = $_POST['ngayden'];
		$gioden = $_POST['gioden'];
		$phutden = $_POST['phutden'];
		if((checkdate($_POST['thangden'], $_POST['ngayden'], $_POST['namden'])==FALSE) or ($gioden>24) or($gioden<0) or ($phutden>59) or ($phutden<0) or (mktime($giodi,$phutdi,00,$thangdi,$ngaydi,$namdi) >= mktime($gioden,$phutden,00,$thangden,$ngayden,$namden)))
		{
			$validated = 0;
			$thoigiandenErr = "Thời gian đến đã nhập không hợp lệ hoặc trước thời gian đi";
		}

	}
}
?>
<?php
if($searchsuccess == 1)
{
	?>
	<form action="updatechuyenbay.php" method="post" name="form1" id="form1">
<div style="border:#F00 solid 1px; width:300px; margin:auto">
<div style="background:#F00; color:#FFF; text-align:center; padding: 5px 0px 5px 0px"><strong>Sửa Thông Tin Chuyến Bay</strong></div>
        <table width="255" align="center">
        	<tr valign="baseline">
            <td nowrap="nowrap" align="right">Chuyến bay ID :</td>
            <td><input type="text" name="chuyenbayid" value=<?php echo "\"".$chuyenbayid."\""?> size="20" readonly/></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Điểm rời :</td>
            <td><input type="text" name="diemroi" value=<?php echo "\"".$diemroi."\""?> size="20" readonly/></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Điểm đến :</td>
            <td><input type="text" name="diemden" value=<?php echo "\"".$diemden."\""?> size="20" readonly/></td>
          </tr>
          <tr valign="baseline" >
            <td nowrap="nowrap" align="right" <?php if ($chuyenbayden == true) echo "hidden"?> >Cửa đi:</td>
            <td><select name="cuadi" <?php if ($chuyenbayden == true) echo "hidden"?>> 
                <option <?php if($cuadi == "D1") echo "\"selected\""?> value = "D1" > D1 </option>
                <option <?php if($cuadi == "D2") echo "\"selected\""?> value = "D2" > D2 </option>
                <option <?php if($cuadi == "D3") echo "\"selected\""?> value = "D3" > D3 </option>
                <option <?php if($cuadi == "D4") echo "\"selected\""?> value = "D4" > D4 </option>
                <option <?php if($cuadi == "D5") echo "\"selected\""?> value = "D5" > D5 </option>
                </select><br /></td>
          </tr
          <tr valign="baseline" >
            <td nowrap="nowrap" align="right" <?php if ($chuyenbayden != true) echo "hidden"?>>Cửa đến:</td>
            <td><select name="cuaden" <?php if ($chuyenbayden != true) echo "hidden"?>> 
                <option <?php if($cuaden == "A1") echo "\"selected\""?> value = "A1" > A1 </option>
                <option <?php if($cuaden == "A2") echo "\"selected\""?> value = "A2" > A2 </option>
                <option <?php if($cuaden == "A3") echo "\"selected\""?> value = "A3" > A3 </option>
                <option <?php if($cuaden == "A4") echo "\"selected\""?> value = "A4" > A4 </option>
                <option <?php if($cuaden == "A5") echo "\"selected\""?> value = "A5" > A5 </option>
                </select><br /></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Máy bay :</td>
            <td><input type="text" name="maybay" value=<?php echo "\"".$maybay."\""?> size="20" /><span class="error"><?php echo "<br />".$maybayErr;?></span></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Số ghế tối đa :</td>
            <td><input type="number" name="soghetoida" value=<?php echo "\"".$soghetoida."\""?> size="20" /><span class="error"><?php echo "<br />".$soghetoidaErr;?></span></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Số ghế đã đặt :</td>
            <td><input type="number" name="soghedadat" value=<?php echo "\"".$soghedadat."\""?> size="20" readonly/></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Thời gian đi :</td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Năm :</td>
            <td><input type="number" name="namdi" value=<?php echo "\"".$namdi."\""?> size="20" /></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Tháng:</td>
            <td><input type="number" name="thangdi" value=<?php echo "\"".$thangdi."\""?> size="20" /></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Ngày:</td>
            <td><input type="number" name="ngaydi" value=<?php echo "\"".$ngaydi."\""?> size="20" /></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Giờ:</td>
            <td><input type="number" name="giodi" value=<?php echo "\"".$giodi."\""?> size="20" /></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Phút:</td>
            <td><input type="number" name="phutdi" value=<?php echo "\"".$phutdi."\""?> size="20" /><span class="error"><?php echo "<br />".$thoigiandiErr;?></span></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Thời gian đến :</td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Năm:</td>
            <td><input type="number" name="namden" value=<?php echo "\"".$namden."\""?> size="20" /></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Tháng:</td>
            <td><input type="number" name="thangden" value=<?php echo "\"".$thangden."\""?> size="20" /></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Ngày:</td>
            <td><input type="number" name="ngayden" value=<?php echo "\"".$ngayden."\""?> size="20" /></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Giờ:</td>
            <td><input type="number" name="gioden" value=<?php echo "\"".$gioden."\""?> size="20" /></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Phút:</td>
            <td><input type="number" name="phutden" value=<?php echo "\"".$phutden."\""?> size="20" /><span class="error"><?php echo "<br />".$thoigiandenErr;?></span></td>
          </tr>
          <tr valign="baseline">
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">&nbsp;</td>
            <td><input name="submit" type="submit" value="Update" /></td>
            <td><input name="submit" type="submit" value="Hủy" /></td>
          </tr>
        </table>
</div>
</form>
	<?php
}
if($searchsuccess==0)
{
?>
<form action="updatechuyenbay.php" method="post" name="form1" id="form1">
<div style="border:#F00 solid 1px; width:300px; margin:auto">
<div style="background:#F00; color:#FFF; text-align:center; padding: 5px 0px 5px 0px"><strong>Sửa Thông Tin Chuyến Bay</strong></div>
        <table width="255" align="center">
        	<tr valign="baseline">
            <td nowrap="nowrap" align="right">Chuyến bay ID :</td>
            <td><input type="text" name="chuyenbayid" value="" size="20" /><span class="error"><?php echo "<br />".$chuyenbayidErr;?></span></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">&nbsp;</td>
            <td><input name="submit" type="submit" value="Tìm Kiếm" /></td>
            <td><input name="submit" type="submit" value="Hủy" /></td>
          </tr>
        </table>
</div>
</form>
<?php
}
if($validated == 1)
{
	$updatehk = pg_query("UPDATE chuyenbay SET maybay = '".$maybayupdate."', soghetoida = '".$soghetoidaupdate."', cuadi = '".$cuadi."', cuaden = '".$cuaden."', thoigiandi = make_timestamp(".$namdi.", ".$thangdi.", ".$ngaydi.", ".$giodi.", ".$phutdi.", 00 :: double precision), thoigianden = make_timestamp(".$namden.", ".$thangden.", ".$ngayden.", ".$gioden.", ".$phutden.", 00 :: double precision) WHERE chuyenbayid = '".$chuyenbayid."'");
	$sql = pg_query("SELECT * FROM chuyenbay WHERE chuyenbayid = '".$chuyenbayid."'");
?>
<table class="tablebg" border="0" width="800" align="center" cellpadding="1" cellspacing="1">
  <tr>
    <th width="80" rowspan="1" align="center">Chuyến bay ID</th>
    <th width="220" rowspan="1" align="center">Điểm rời</th>
    <th width="90" rowspan="1" align="center">Điểm đến</th>
    <th width="90" rowspan="1" align="center">Cửa đến</th>
    <th width="90" rowspan="1" align="center">Cửa đi</th>
    <th width="90" rowspan="1" align="center">Hãng bay ID</th>
    <th width="90" rowspan="1" align="center">Máy bay</th>
    <th width="90" rowspan="1" align="center">Còn chỗ</th>
    <th width="90" rowspan="1" align="center">Số ghế tối đa</th>
    <th width="90" rowspan="1" align="center">Số ghế đã đặt</th>
    <th width="90" rowspan="1" align="center">Thời gian đi</th>
    <th width="90" rowspan="1" align="center">Thời gian đến</th>
  </tr>
  <?php while ($row_RCdanh_sach = pg_fetch_assoc($sql)){ ?>
    <tr class="row">
      <td  class="row1" align="center"><?php echo $row_RCdanh_sach['chuyenbayid']; ?></td>
      <td  class="row1" align="center"><?php echo $row_RCdanh_sach['diemroi']; ?></td>
      <td  class="row1" align="center"><?php echo $row_RCdanh_sach['diemden']; ?></td>
      <td  class="row1" align="center"><?php echo $row_RCdanh_sach['cuaden']; ?></td>
      <td  class="row1" align="center"><?php echo $row_RCdanh_sach['cuadi']; ?></td>
      <td  class="row1" align="center"><?php echo $row_RCdanh_sach['hangbayid']; ?></td>
      <td  class="row1" align="center"><?php echo $row_RCdanh_sach['maybay']; ?></td>
      <td  class="row1" align="center"><?php echo $row_RCdanh_sach['concho']; ?></td>
      <td  class="row1" align="center"><?php echo $row_RCdanh_sach['soghetoida']; ?></td>
      <td  class="row1" align="center"><?php echo $row_RCdanh_sach['soghedadat']; ?></td>
      <td  class="row1" align="center"><?php echo $row_RCdanh_sach['thoigiandi']; ?></td>
      <td  class="row1" align="center"><?php echo $row_RCdanh_sach['thoigianden']; ?></td>
    </tr>
    <?php }  ?>
</table>
<?php
}
?>
</body>
</html>