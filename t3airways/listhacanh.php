<?php 
session_start();
require_once('Connections/Myconnection.php');
if (($_SESSION['logged-in']!=true) or ($_SESSION['user_name']!='admin')) {
  header("Location: Index.php");
}
require_once('admin_sanbay_tool.php');
?>
<?php
$sql = pg_query("select chuyenbayid, diemroi, cuaden, hangbayid, thoigiandi, thoigianden
 from chuyenbay
 where thoigianden > localtimestamp and thoigianden < localtimestamp + interval '1 hour' and diemden = 'Hanoi'
 order by thoigianden asc limit 10 offset 0")

?>
<center>Danh sách các chuyến bay sắp hạ cánh</center>
<table class="tablebg" border="0" width="800" align="center" cellpadding="1" cellspacing="1">
  <tr>
    <th width="80" rowspan="1" align="center">Chuyến bay ID</th>
    <th width="220" rowspan="1" align="center">Điểm rời</th>
    <th width="220" rowspan="1" align="center">Cửa đến</th>
    <th width="90" rowspan="1" align="center">Hãng bay ID</th>
    <th width="90" rowspan="1" align="center">Thời gian đi</th>
    <th width="90" rowspan="1" align="center">Thời gian đến</th>
  </tr>
  <?php while ($row_RCdanh_sach = pg_fetch_assoc($sql)){ ?>
    <tr class="row">
      <td  class="row1" align="center"><?php echo $row_RCdanh_sach['chuyenbayid']; ?></td>
      <td  class="row1" align="center"><?php echo $row_RCdanh_sach['diemroi']; ?></td>
      <td  class="row1" align="center"><?php echo $row_RCdanh_sach['cuaden']; ?></td>
      <td  class="row1" align="center"><?php echo $row_RCdanh_sach['hangbayid']; ?></td>
      <td  class="row1" align="center"><?php echo $row_RCdanh_sach['thoigiandi']; ?></td>
      <td  class="row1" align="center"><?php echo $row_RCdanh_sach['thoigianden']; ?></td>
    </tr>
    <?php }  ?>
</table>
</body>
</html>