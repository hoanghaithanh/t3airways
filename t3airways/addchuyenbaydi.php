<?php 
require_once('Connections/Myconnection.php');
session_start(); 
require_once('admin_hangbay_tool.php');
if (($_SESSION['logged-in']!=true)) {
  header("Location: Index.php");

}
?>
	<?php
	$diemroi = "Hanoi";
	$concho = FALSE;
	$diemdenErr = $maybayErr = $soghetoidaErr = $thoigiandiErr = $thoigiandenErr = $soghedadatErr =  "";
	$diemden = $cuaden = $maybay = $soghetoida = $thoigiandi = $thoigianden = "";
	$validated = 0;
	if ($_SERVER["REQUEST_METHOD"]=="POST") {
		
		$validated = 1;
		$hangbayid = $_SESSION['user_name'];

    if (empty($_POST['chuyenbayid'])) {
      $chuyenbayidErr = "Bạn chưa nhập chuyến bay ID";
      $validated=0;
    } else {
      $chuyenbayid = $_POST['chuyenbayid'];
      $maso = substr($chuyenbayid,3,7);
      if((substr($chuyenbayid,0,3) != $hangbayid) or !($maso <= 99999 and $maso >= 0) or strlen($chuyenbayid) !=8) 
      {
        $validated = 0;
        $chuyenbayidErr = "ID chuyến bay phải có dạng ".$hangbayid."xxxxx, x là các chữ số";
      } else 
      {
        $checkid = pg_query("select * from chuyenbay where chuyenbayid = '".$chuyenbayid."'");
        if (pg_num_rows($checkid)>=1)
        {
          $validated = 0;
          $chuyenbayidErr = "Đã tồn tại ID này trong dữ liệu";
        }
      }
    }

		if (empty($_POST['diemden'])) {
			$diemdenErr = "Bạn chưa nhập điểm đến";
			$validated=0;
		} else {
			$diemden = $_POST['diemden'];
		}

		if (empty($_POST['maybay'])) {
			$maybayErr = "Bạn chưa nhập loại máy bay";
			$validated = 0;
		} else {
			$maybay = $_POST['maybay'];
		}

		if (empty($_POST['soghetoida'])) {
			$soghetoidaErr = "Số ghế tối đa không hợp lệ";
			$validated=0;
		} else {
			$soghetoida = $_POST['soghetoida'];
		}

		$cuadi = $_POST['cuadi'];

		if (checkdate($_POST['thangdi'], $_POST['ngaydi'], $_POST['namdi'])==FALSE)
		{
			$thoigiandiErr="Ngày tháng đi không hợp lệ";
			$validated=0;
		} else {
			$namdi = $_POST['namdi'];
			$ngaydi = $_POST['ngaydi'];
			$thangdi = $_POST['thangdi'];
		}

		if (checkdate($_POST['thangden'], $_POST['ngayden'], $_POST['namden'])==FALSE)
		{
			$thoigiandenErr="Ngày tháng đến không hợp lệ";
			$validated=0;
		} else {
			$namden = $_POST['namden'];
			$ngayden = $_POST['ngayden'];
			$thangden = $_POST['thangden'];
		}
		$giodi = $_POST['giodi'];
		$phutdi = $_POST['phutdi'];
		$gioden = $_POST['gioden'];
		$phutden = $_POST['phutden'];
    $timecheck = $namdi."-".$thangdi."-".$ngaydi." ".$giodi.":".$phutdi.":00";
    if (date("Y-m-d H:i:s") > $timecheck)
    {
      $thoigiandiErr = "Thời gian đi không được xảy ra trước thời điểm hiện tại";
      $validated = 0;
    }
		if(mktime($giodi,$phutdi,00,$thangdi,$ngaydi,$namdi) >= mktime($gioden,$phutden,00,$thangden,$ngayden,$namden))
		{
			$validated = 0;
			$thoigianErr = "Thời gian đi nằm sau thời gian đến!";
		}
		$soghedadat = 0;
	}
	?>
<form action="addchuyenbaydi.php" method="post" name="form1" id="form1">
<div style="border:#F00 solid 1px; width:300px; margin:auto">
<div style="background:#F00; color:#FFF; text-align:center; padding: 5px 0px 5px 0px"><strong>Thêm chuyến bay đi</strong></div>
        <table width="255" align="center">
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Mã chuyến bay:</td>
            <td><input type="text" name="chuyenbayid" value="" size="24" /><?php echo "<br />".$chuyenbayidErr;?></span></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Điểm đến:</td>
            <td><input type="text" name="diemden" value="" size="24" /><?php echo "<br />".$diemdenErr;?></span></td>
          </tr>
           <tr valign="baseline">
            <td nowrap="nowrap" align="right">Số ghế tối đa:</td>
            <td><input type="text" name="soghetoida" value="" size="24" /><span class="error"><?php echo "<br />".$soghetoidaErr;?></span></td>
          </tr>
           <tr valign="baseline">
            <td nowrap="nowrap" align="right">Máy bay:</td>
            <td><input type="text" name="maybay" value="" size="24" /><span class="error"><?php echo "<br />".$maybayErr;?></span></td>
          </tr>
           <tr valign="baseline">
            <td nowrap="nowrap" align="right">Cửa đi:</td>
            <td><select name="cuadi"> 
                <option value = "D1" > D1 </option>
                <option value = "D2" > D2 </option>
                <option value = "D3" > D3 </option>
                <option value = "D4" > D4 </option>
                <option value = "D5" > D5 </option>
                </select><br /></td>
          </tr>
           <tr valign="baseline">
            <td nowrap="nowrap" align="right">Thời gian đi:</td>
          </tr>
            <tr valign="baseline">
            <td nowrap="nowrap" align="right">Năm:</td>
            <td><input type="number" name="namdi" value="2015" min="2015" size="24" /></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Tháng:</td>
            <td><input type="number" name="thangdi" value="1" max="12" min="1" size="24" /></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Ngày:</td>
            <td><input type="number" name="ngaydi" value="1" max="31" min="1" size="24" /></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Giờ:</td>
            <td><input type="number" name="giodi" value="0" min="0" max="24" size="24" /></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Phút:</td>
            <td><input type="number" name="phutdi" value="0" min="0" max="60" size="24" /><span class="error"><?php echo "<br />".$thoigiandiErr;?></span></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Thời gian đến:</td>
          </tr>
            <tr valign="baseline">
            <td nowrap="nowrap" align="right">Năm:</td>
            <td><input type="number" name="namden" value="2015" min="2015" size="24" /></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Tháng:</td>
            <td><input type="number" name="thangden" value="1" max="12" min="1" size="24" /></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Ngày:</td>
            <td><input type="number" name="ngayden" value="1" max="31" min="1" size="24" /></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Giờ:</td>
            <td><input type="number" name="gioden" value="0" min="0" max="24" size="24" /></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Phút:</td>
            <td><input type="number" name="phutden" value="0" min="0" max="60" size="24" /><span class="error"><?php echo "<br />".$thoigiandenErr;?></span><span class="error"><?php echo "<br />".$thoigianErr;?></span></td>
          </tr>
          <tr valign="baseline">
            <td height="26" align="right" nowrap="nowrap">&nbsp;</td>
            <td><input name="submit" type="submit" value="Thêm" /></td>
          </tr>
        </table>
</div>
</form>


<?php
	if ($validated == 1) {
$sql1=pg_query("INSERT INTO chuyenbay(chuyenbayid, diemroi, diemden, cuadi, hangbayid, maybay, soghetoida, soghedadat, thoigiandi, thoigianden) 
	VALUES ('".$chuyenbayid."', '".$diemroi."', '".$diemden."', '".$cuadi."', '".$hangbayid."', '".$maybay."', ".$soghetoida." :: int, ".$soghedadat." :: int, make_timestamp(".$namdi.", ".$thangdi.", ".$ngaydi.", ".$giodi.", ".$phutdi.", 00 :: double precision), make_timestamp(".$namden.", ".$thangden.", ".$ngayden.", ".$gioden.", ".$phutden.", 00 :: double precision))");

$sql=pg_query("SELECT * FROM chuyenbay WHERE chuyenbayid = '".$chuyenbayid."'");
?>
<table class="tablebg" border="0" width="800" align="center" cellpadding="1" cellspacing="1">
  <tr>
    <th width="80" rowspan="1" align="center">Chuyến bay ID</th>
    <th width="220" rowspan="1" align="center">Điểm rời</th>
    <th width="90" rowspan="1" align="center">Điểm đến</th>
    <th width="90" rowspan="1" align="center">Cửa đến</th>
    <th width="90" rowspan="1" align="center">Cửa đi</th>
    <th width="90" rowspan="1" align="center">Hãng bay ID</th>
    <th width="90" rowspan="1" align="center">Máy bay</th>
    <th width="90" rowspan="1" align="center">Còn chỗ</th>
    <th width="90" rowspan="1" align="center">Số ghế tối đa</th>
    <th width="90" rowspan="1" align="center">Số ghế đã đặt</th>
    <th width="90" rowspan="1" align="center">Thời gian đi</th>
    <th width="90" rowspan="1" align="center">Thời gian đến</th>
  </tr>
  <?php while ($row_RCdanh_sach = pg_fetch_assoc($sql)){ ?>
    <tr class="row">
      <td  class="row1" align="left"><?php echo $row_RCdanh_sach['chuyenbayid']; ?></td>
      <td  class="row1" align="left"><?php echo $row_RCdanh_sach['diemroi']; ?></td>
      <td  class="row1" align="left"><?php echo $row_RCdanh_sach['diemden']; ?></td>
      <td  class="row1" align="left"><?php echo $row_RCdanh_sach['cuaden']; ?></td>
      <td  class="row1" align="left"><?php echo $row_RCdanh_sach['cuadi']; ?></td>
      <td  class="row1" align="left"><?php echo $row_RCdanh_sach['hangbayid']; ?></td>
      <td  class="row1" align="left"><?php echo $row_RCdanh_sach['maybay']; ?></td>
      <td  class="row1" align="left"><?php echo $row_RCdanh_sach['concho']; ?></td>
      <td  class="row1" align="left"><?php echo $row_RCdanh_sach['soghetoida']; ?></td>
      <td  class="row1" align="left"><?php echo $row_RCdanh_sach['soghedadat']; ?></td>
      <td  class="row1" align="left"><?php echo $row_RCdanh_sach['thoigiandi']; ?></td>
      <td  class="row1" align="left"><?php echo $row_RCdanh_sach['thoigianden']; ?></td>
    </tr>
    <?php }  ?>
</table>
<?php
}
?>
</body>
</html>