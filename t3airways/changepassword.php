<?php 
session_start(); 
require_once('Connections/Myconnection.php');
if (($_SESSION['logged-in']!=true)) {
  header("Location: Index.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Thay đổi mật khẩu</title>
	</head>

	<body style="margin-top:100px;">
		<?php
		$validated = 0;
		$oldpassErr = $newpassErr = $againnewpassErr = "";
		$oldpass = $newpass = $againnewpass = "";
		if($_POST['submit']=="Hủy") {
			header("Location: index.php");
			exit;
		}
		if($_POST['submit']=="Thay đổi") 
		{
			$validated = 1;
			if(empty($_POST['oldpass']))
			{
				$oldpassErr = "Bạn chưa nhập mật khẩu hiện tại";
				$validated = 0;
			}
			else $oldpass = md5($_POST['oldpass']);

			if(empty($_POST['newpass']))
			{
				$newpassErr = "Bạn chưa nhập mật khẩu mới";
				$validated = 0;
			}

			if($_POST['newpass'] == $_POST['oldpass'])
			{
				$newpassErr = "Mật khẩu mới phải khác mật khẩu cũ";
				$validated = 0;
			}

			if(strlen($_POST['newpass']) < 5) 
			{
				$newpassErr = "Mật khẩu phải có ít nhất 5 ký tự";
				$validated = 0;
			}

			else $newpass = md5($_POST['newpass']);

			if(empty($_POST['againnewpass']) or ($_POST['againnewpass']!=$_POST['newpass']))
			{
				$againnewpassErr = "Mật khẩu mới không khớp";
				$validated = 0;
			}
			else $againnewpass = md5($_POST['againnewpass']);

			if($_SESSION['user_name'] == 'admin')
			{
				$result = pg_query("select * from admin_sanbay where username = 'admin'");
				$rows = pg_fetch_array($result);
				if(md5($rows['password'])!=$oldpass)
				{
					$oldpassErr = "Mật khẩu đã nhập không chính xác!";
					$validated = 0;
				}

			}
			else 
			{
				$result = pg_query("select * from hangbay where hangbayid = '".$_SESSION['user_name']."'");
				$rows = pg_fetch_array($result);
				if(md5($rows['matkhau'])!=$oldpass)
				{
					$oldpassErr = "Mật khẩu đã nhập không chính xác!";
					$validated = 0;
				}
			}
		}
		?>
		<form action="changepassword.php" method="post" name="form1" id="form1">
			<div style="border:#F00 solid 1px; width:400px; margin:auto">
			<div style="background:#F00; color:#FFF; text-align:center; padding: 5px 0px 5px 0px"><strong>Thay đổi mật khẩu</strong></div>
        		<table width="255" align="center">
          			<tr valign="baseline">
            			<td nowrap="nowrap" align="right">Mật khẩu cũ:</td>
            			<td><input type="password" name="oldpass" value="" size="24" /></td>
          			</tr>
          			<tr valign="baseline">
            			<td nowrap="nowrap" align="right">Mật khẩu mới:</td>
            			<td><input type="password" name="newpass" value="" size="24" /></td>
          			</tr>
          			<tr valign="baseline">
            			<td nowrap="nowrap" align="right">Nhập lại mật khẩu mới:</td>
            			<td><input type="password" name="againnewpass" value="" size="24" /></td>
          			</tr>
          			<tr valign="baseline">
            			<td nowrap="nowrap" align="right">&nbsp;</td>
           				<td><input name="submit" type="submit" value="Thay đổi" /></td>
           				<td><input name="submit" type="submit" value="Hủy" /></td>
          			</tr>
        		</table>
			</div>
		</form>
		<?php echo "<center>".$oldpassErr."</center><br /><center>".$newpassErr."</center><br /><center>".$againnewpassErr."</center>";?>
		<?php 
			if($validated == 1)
			{
				if($_SESSION['user_name']=='admin')
				{
					$changepass = pg_query("update admin_sanbay set password = '".$_POST['newpass']."' where username = 'admin'");
					if ($changepass)
					{
						echo "<center>Đổi mật khẩu thành công!</center>";
					}
					else echo "<center>Đã xảy ra sự cố, đổi mật khẩu thất bại!</center>";
				}

				else
				{
					$changepass = pg_query("update hangbay set matkhau = '".$_POST['newpass']."' where username = '".$_SESSION['user_name']."'");
					if ($changepass)
					{
						echo "<center>Đổi mật khẩu thành công!</center>";
					}
					else echo "<center>Đã xảy ra sự cố, đổi mật khẩu thất bại!</center>";
				}
			}
		?>
	</body>
</html>