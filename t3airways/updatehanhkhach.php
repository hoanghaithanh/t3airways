<?php 
session_start();
require_once('Connections/Myconnection.php');
if (($_SESSION['logged-in']!=true) or ($_SESSION['user_name']!='admin')) {
  header("Location: Index.php");
}
require_once('admin_sanbay_tool.php');
?>
<?php

$hanhkhachid = $hoten = $cmnd = $diachi = $ngaysinh = "";
$hotenupdate = $cmndupdate = $diachiupdate = $ngaysinhupdate = "";
$hanhkhachidErr = $hotenErr = $cmndErr = $diachiErr = $ngaysinhErr = "";
$searchsuccess = 0;
$validated = 0;
$submit = $_POST['submit'];
if($submit == "Hủy")
{
	header("Location: index.php");
}
if($submit=="Tìm Kiếm")
{

	if(empty($_POST['hanhkhachid']))
	{
		$validated = 0;
		$hanhkhachidErr = "Bạn phải nhập ID khách hàng cần sửa thông tin";
	}
	else $hanhkhachid = $_POST['hanhkhachid'];
	$sql1 = pg_query("SELECT * FROM hanhkhach WHERE hanhkhachid = '".$hanhkhachid."'");
	$confirm = pg_num_rows($sql1);
	if($confirm >= 1)
	{
		$searchsuccess = 1;
		$row_RCdanh_sach = pg_fetch_assoc($sql1);
		$hoten = $row_RCdanh_sach['hoten'];
		$ngaysinh = $row_RCdanh_sach['ngaysinh'];
		$namsinh = substr($ngaysinh,0,4);
		$thangsinh = substr($ngaysinh,5,2);
		$ngaysinh = substr($ngaysinh,8,2);
		$cmnd = $row_RCdanh_sach['cmnd'];
		$diachi = $row_RCdanh_sach['diachi'];
	}
	else $hanhkhachidErr = "Không tồn tại ID đã nhập!";
}
if($submit == "Update")
{	
	$hanhkhachid = $_POST['hanhkhachid'];
	$searchsuccess = 1;
	$validated = 1;
	if(empty($_POST['hoten']))
	{
		$validated = 0;
		$hotenErr = "Bạn chưa điền họ tên!";
	}
	else $hoten = $hotenupdate = $_POST['hoten'];

	if(empty($_POST['diachi']))
	{
		$validated = 0;
		$diachiErr = "Bạn chưa điền địa chỉ!";
	}
	else $diachi = $diachiupdate = $_POST['diachi'];

	if(strlen($_POST['cmnd'])!=12)
	{
		$cmnd = $_POST['cmnd'];
		$validated = 0;
		$cmndErr = "CMND không đủ 12 ký tự!";
	}
	else 
		{
			$checkcmnd = pg_query("SELECT * FROM hanhkhach WHERE hanhkhachid != '".$_POST['hanhkhachid']."' and cmnd = '".$_POST['cmnd']."'");
			if(pg_num_rows($checkcmnd) > 0)
			{
				$cmnd = $_POST['cmnd'];
				$validated = 0;
				$cmndErr = "Đã tồn tại số CMND này trong danh sách khách hàng!";
			}
			 else $cmnd = $cmndupdate = $_POST['cmnd'];
		}

	if(empty($_POST['namsinh']) or empty($_POST['thangsinh']) or empty($_POST['ngaysinh']))
	{
		$validated = 0;
		$ngaysinhErr = "Bạn chưa điền đủ ngày sinh!";
	}
	else 
	{
		$namsinh = $_POST['namsinh'];
		$thangsinh = $_POST['thangsinh'];
		$ngaysinh = $_POST['ngaysinh'];
		if(checkdate($_POST['thangsinh'], $_POST['ngaysinh'], $_POST['namsinh'])==FALSE)
		{
			$validated = 0;
			$ngaysinhErr = "Ngày sinh đã nhập không hợp lệ";
		}
		else $ngaysinh = $ngaysinhupdate = $_POST['namsinh']."-".$_POST['thangsinh']."-".$_POST['ngaysinh'];
	}
}
?>
<?php
if($searchsuccess == 1)
{
?>
<form action="updatehanhkhach.php" method="post" name="form1" id="form1">
<div style="border:#F00 solid 1px; width:300px; margin:auto">
<div style="background:#F00; color:#FFF; text-align:center; padding: 5px 0px 5px 0px"><strong>Sửa Thông Tin Khách Hàng</strong></div>
        <table width="255" align="center">
        	<tr valign="baseline">
            <td nowrap="nowrap" align="right">Khách Hàng ID :</td>
            <td><input type="text" name="hanhkhachid" value=<?php echo "\"".$hanhkhachid."\""?> size="20" readonly/><span class="error"><?php echo "<br />".$hanhkhachidErr;?></span></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Họ Tên :</td>
            <td><input type="text" name="hoten" value=<?php echo "\"".$hoten."\""?> size="20" /><span class="error"><?php echo "<br />".$hotenErr;?></span></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">CMND:</td>
            <td><input type="text" name="cmnd" value=<?php echo "\"".$cmnd."\""?> size="20" /><span class="error"><?php echo "<br />".$cmndErr;?></span></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Địa Chỉ:</td>
            <td><input type="text" name="diachi" value=<?php echo "\"".$diachi."\""?> size="20" /><span class="error"><?php echo "<br />".$diachiErr;?></span></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Năm sinh:</td>
            <td><input type="text" name="namsinh" value=<?php echo "\"".$namsinh."\""?> size="20" />
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Tháng sinh:</td>
            <td><input type="text" name="thangsinh" value=<?php echo "\"".$thangsinh."\""?> size="20" />
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Ngày sinh:</td>
            <td><input type="text" name="ngaysinh" value=<?php echo "\"".$ngaysinh."\""?> size="20" />
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">&nbsp;</td>
            <td><input name="submit" type="submit" value="Update" /></td>
            <td><input name="submit" type="submit" value="Hủy" /></td>
          </tr>
        </table>
</div>
</form>
<?php
}
if($searchsuccess==0)
{
?>
<form action="updatehanhkhach.php" method="post" name="form1" id="form1">
<div style="border:#F00 solid 1px; width:300px; margin:auto">
<div style="background:#F00; color:#FFF; text-align:center; padding: 5px 0px 5px 0px"><strong>Sửa Thông Tin Khách Hàng</strong></div>
        <table width="255" align="center">
        	<tr valign="baseline">
            <td nowrap="nowrap" align="right">Khách Hàng ID :</td>
            <td><input type="text" name="hanhkhachid" value="" size="20" /><span class="error"><?php echo "<br />".$hanhkhachidErr;?></span></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">&nbsp;</td>
            <td><input name="submit" type="submit" value="Tìm Kiếm" /></td>
            <td><input name="submit" type="submit" value="Hủy" /></td>
          </tr>
        </table>
</div>
</form>
<?php
}
if($validated == 1)
{
	$updatehk = pg_query("UPDATE hanhkhach SET hoten = '".$hotenupdate."', diachi = '".$diachiupdate."', cmnd = '".$cmndupdate."', ngaysinh = '".$ngaysinhupdate."' WHERE hanhkhachid = '".$hanhkhachid."'");
	$sql = pg_query("SELECT * FROM hanhkhach WHERE hanhkhachid = '".$hanhkhachid."'");
?>
<table class="tablebg" border="0" width="800" align="center" cellpadding="1" cellspacing="1">
  <tr>
    <th width="80" rowspan="1" align="center">Khách hàng ID</th>
    <th width="220" rowspan="1" align="center">Họ tên</th>
    <th width="220" rowspan="1" align="center">Ngày sinh</th>
    <th width="90" rowspan="1" align="center">CMND</th>
    <th width="90" rowspan="1" align="center">Địa chỉ</th>
  </tr>
  <?php while ($row_RCdanh_sach = pg_fetch_assoc($sql)){ ?>
    <tr class="row">
      <td  class="row1" align="left"><?php echo $row_RCdanh_sach['hanhkhachid']; ?></td>
      <td  class="row1" align="left"><?php echo $row_RCdanh_sach['hoten']; ?></td>
      <td  class="row1" align="left"><?php echo $row_RCdanh_sach['ngaysinh']; ?></td>
      <td  class="row1" align="left"><?php echo $row_RCdanh_sach['cmnd']; ?></td>
      <td  class="row1" align="left"><?php echo $row_RCdanh_sach['diachi']; ?></td>
    </tr>
    <?php }  ?>
</table>
<?php
}
?>
</body>
</html>