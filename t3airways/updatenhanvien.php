<?php 
session_start();
require_once('Connections/Myconnection.php');
if (($_SESSION['logged-in']!=true) or ($_SESSION['user_name']=='admin')) {
  header("Location: Index.php");
}
require_once('admin_hangbay_tool.php');
?>
<?php


$hotenupdate = $ngaysinhupdate = "";
$hotenErr = $ngaysinhErr = "";
$searchsuccess = 0;
$validated = 0;
$submit = $_POST['submit'];
if($submit == "Hủy")
{
	header("Location: index.php");
}
if($submit=="Tìm Kiếm")
{
	$nhanvienid = $hoten = $ngaysinh = "";
	if(empty($_POST['nhanvienid']))
	{
		$validated = 0;
		$nhanvienidErr = "Bạn phải nhập ID nhân viên cần sửa thông tin";
	}
	else $nhanvienid = $_POST['nhanvienid'];
	$sql1 = pg_query("SELECT * FROM nhanvien WHERE nhanvienid = '".$nhanvienid."' and hangbayid = '".$_SESSION['user_name']."'");
	$confirm = pg_num_rows($sql1);
	if($confirm >= 1)
	{
		$searchsuccess = 1;
		$row_RCdanh_sach = pg_fetch_assoc($sql1);
		$hoten = $row_RCdanh_sach['hoten'];
		$ngaysinh = $row_RCdanh_sach['ngaysinh'];
		$nam = substr($ngaysinh,0,4);
		$thang = substr($ngaysinh,5,2);
		$ngay = substr($ngaysinh,8,2);
	}
	else $nhanvienidErr = "Không tồn tại ID đã nhập hoặc nhân viên không thuộc hãng bay!";
}
if($submit == "Update")
{	
	$nhanvienid = $_POST['nhanvienid'];
	$searchsuccess = 1;
	$validated = 1;
	if(empty($_POST['hoten']))
	{
		$validated = 0;
		$hotenErr = "Bạn chưa điền họ tên!";
	}
	else $hoten = $hotenupdate = $_POST['hoten'];

	

	if(empty($_POST['namsinh']) or empty($_POST['thangsinh']) or empty($_POST['ngaysinh']))
	{
		$validated = 0;
		$ngaysinhErr = "Bạn chưa điền đủ ngày sinh!";
	}
	else 
	{
		$nam = $_POST['namsinh'];
		$thang = $_POST['thangsinh'];
		$ngay = $_POST['ngaysinh'];
		if(checkdate($_POST['thangsinh'], $_POST['ngaysinh'], $_POST['namsinh'])==FALSE)
		{
			$validated = 0;
			$ngaysinhErr = "Ngày sinh đã nhập không hợp lệ";
		}
		else $ngaysinh = $ngaysinhupdate = $_POST['namsinh']."-".$_POST['thangsinh']."-".$_POST['ngaysinh'];
	}
}
?>
<?php
if($searchsuccess == 1)
{
?>
<form action="updatenhanvien.php" method="post" name="form1" id="form1">
<div style="border:#F00 solid 1px; width:300px; margin:auto">
<div style="background:#F00; color:#FFF; text-align:center; padding: 5px 0px 5px 0px"><strong>Sửa Thông Tin Nhân Viên</strong></div>
        <table width="255" align="center">
        	<tr valign="baseline">
            <td nowrap="nowrap" align="right">Nhân Viên ID :</td>
            <td><input type="text" name="nhanvienid" value=<?php echo "\"".$nhanvienid."\""?> size="20" readonly/><span class="error"><?php echo "<br />".$nhanvienidErr;?></span></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Họ Tên :</td>
            <td><input type="text" name="hoten" value=<?php echo "\"".$hoten."\""?> size="20" /><span class="error"><?php echo "<br />".$hotenErr;?></span></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Năm sinh:</td>
            <td><input type="text" name="namsinh" value=<?php echo "\"".$nam."\""?> size="20" />
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Tháng sinh:</td>
            <td><input type="text" name="thangsinh" value=<?php echo "\"".$thang."\""?> size="20" />
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Ngày sinh:</td>
            <td><input type="text" name="ngaysinh" value=<?php echo "\"".$ngay."\""?> size="20" /><span class="error"><?php echo "<br />".$ngaysinhErr;?></span>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">&nbsp;</td>
            <td><input name="submit" type="submit" value="Update" /></td>
            <td><input name="submit" type="submit" value="Hủy" /></td>
          </tr>
        </table>
</div>
</form>
<?php
}
if($searchsuccess==0)
{
?>
<form action="updatenhanvien.php" method="post" name="form1" id="form1">
<div style="border:#F00 solid 1px; width:300px; margin:auto">
<div style="background:#F00; color:#FFF; text-align:center; padding: 5px 0px 5px 0px"><strong>Sửa Thông Tin Nhân Viên</strong></div>
        <table width="255" align="center">
        	<tr valign="baseline">
            <td nowrap="nowrap" align="right">Nhân Viên ID :</td>
            <td><input type="text" name="nhanvienid" value="" size="20" /><span class="error"><?php echo "<br />".$nhanvienidErr;?></span></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">&nbsp;</td>
            <td><input name="submit" type="submit" value="Tìm Kiếm" /></td>
            <td><input name="submit" type="submit" value="Hủy" /></td>
          </tr>
        </table>
</div>
</form>
<?php
}
if($validated == 1)
{
	$updatehk = pg_query("UPDATE nhanvien SET hoten = '".$hotenupdate."',  ngaysinh = '".$ngaysinhupdate."' WHERE nhanvienid = '".$nhanvienid."'");
	$sql = pg_query("SELECT * FROM nhanvien WHERE nhanvienid = '".$nhanvienid."'");
?>
<table class="tablebg" border="0" width="800" align="center" cellpadding="1" cellspacing="1">
  <tr>
    <th width="80" rowspan="1" align="center">Nhân viên ID</th>
    <th width="220" rowspan="1" align="center">Họ tên</th>
    <th width="220" rowspan="1" align="center">Ngày sinh</th>
    <th width="90" rowspan="1" align="center">Hãng Bay</th>
  </tr>
  <?php while ($row_RCdanh_sach = pg_fetch_assoc($sql)){ ?>
    <tr class="row">
      <td  class="row1" align="left"><?php echo $row_RCdanh_sach['nhanvienid']; ?></td>
      <td  class="row1" align="left"><?php echo $row_RCdanh_sach['hoten']; ?></td>
      <td  class="row1" align="left"><?php echo $row_RCdanh_sach['ngaysinh']; ?></td>
      <td  class="row1" align="left"><?php echo $row_RCdanh_sach['hangbayid']; ?></td>
    </tr>
    <?php }  ?>
</table>
<?php
}
?>
</body>
</html>