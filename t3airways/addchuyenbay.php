<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Bootstrap 3 Simple Tables</title>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <style type="text/css">
        .example{
            margin: 20px;
        }
    </style>
</head>
<body>
	<?php
	$diemden = "Hanoi";
	$concho = FALSE;
	$hangbayidErr = $diemroiErr = $cuadenErr = $maybayErr = $soghetoidaErr = $thoigiandiErr = $thoigiandenErr = $soghetoidaErr = $chuyenbayidErr = "";
	$chuyenbayid = $diemroi = $cuaden = $maybay = $soghetoida = $thoigiandi = $thoigianden = "";
	$validated = 0;
	$conn=pg_connect("host=localhost port=5432 dbname=project user=postgres password=hedspi");
	if ($_SERVER["REQUEST_METHOD"]=="POST") {
		
		$validated = 1;
		$hangbayid = "SYN";
		if (empty($_POST['chuyenbayid']) or (strlen($_POST['chuyenbayid'])!=5)) {
			$chuyenbayidErr = "Chuyến bay ID không hợp lệ";
			$validated = 0;
		}	else {
			$chuyenbayid = "SYN".$_POST['chuyenbayid'];

		}

		if (empty($_POST['diemroi'])) {
			$diemroiErr = "Bạn chưa nhập điểm rời";
			$validated=0;
		} else {
			$diemroi = $_POST['diemroi'];
		}

		if (empty($_POST['maybay'])) {
			$maybayErr = "Bạn chưa nhập loại máy bay";
			$validated = 0;
		} else {
			$maybay = $_POST['maybay'];
		}

		if (empty($_POST['soghetoida'])) {
			$soghetoidaErr = "Số ghế tối đa không hợp lệ";
			$validated=0;
		} else {
			$soghetoida = $_POST['soghetoida'];
		}

		$cuaden = $_POST['cuaden'];

		if (checkdate($_POST['thangdi'], $_POST['ngaydi'], $_POST['namdi'])==FALSE)
		{
			$thoigiandiErr="Ngày tháng đi không hợp lệ";
			$validated=0;
		} else {
			$namdi = $_POST['namdi'];
			$ngaydi = $_POST['ngaydi'];
			$thangdi = $_POST['thangdi'];
		}

		if (checkdate($_POST['thangden'], $_POST['ngayden'], $_POST['namden'])==FALSE)
		{
			$thoigiandenErr="Ngày tháng đến không hợp lệ";
			$validated=0;
		} else {
			$namden = $_POST['namden'];
			$ngayden = $_POST['ngayden'];
			$thangden = $_POST['thangden'];
		}
		$giodi = $_POST['giodi'];
		$phutdi = $_POST['phutdi'];
		$gioden = $_POST['gioden'];
		$phutden = $_POST['phutden'];
		if($_POST['soghedadat'] > $_POST['soghetoida']) {
			$soghedadatErr = "Số ghế đã đặt không hợp lệ";
			$validated = 0;
		} else $soghedadat = $_POST['soghedadat'];
		$query= "('".$chuyenbayid."', "."'".$diemroi."', "."'".$diemden."', "."'".$cuaden."', '".$hangbayid."', "."'".$maybay."', ".$soghetoida." :: int, ".$soghedadat." :: int, make_timestamp(".$namdi.", ".$thangdi.", ".$ngaydi.", ".$giodi.", ".$phutdi.", 00 :: double precision), make_timestamp(".$namden.", ".$thangden.", ".$ngayden.", ".$gioden.", ".$phutden.", 00 :: double precision))";
		echo $query;
	}
	?>

<form action="addchuyenbay.php" method="post">
	Chuyến bay ID: <input type="number" name="chuyenbayid" size="20" /><span class="error">*<?php echo $chuyenbayidErr;?></span><br />
	Điểm rời: <input type="text" name="diemroi" size="20" /><span class="error">*<?php echo $diemroiErr;?></span><br />
	Số ghế tối đa: <input type="number" name="soghetoida" size="20" /><span class="error">*<?php echo $soghetoidaErr;?></span><br />
	Số ghế đã đặt: <input type="number" name="soghedadat" size="20" /><span class="error">*<?php echo $soghedadatErr;?></span><br />
	Máy bay: <input type="text" name="maybay" size="20" /><span class="error">*<?php echo $maybayErr;?></span><br />
	Cửa đến: <select name="cuaden"> 
	<option value = "A1" > A1 </option>
	<option value = "A2" > A2 </option>
	<option value = "A3" > A3 </option>
	<option value = "A4" > A4 </option>
	<option value = "A5" > A5 </option>
	</select><br /> 
	<p>Thời gian đi: </p><br />
	Ngày: <input type="number" name="ngaydi" min="1" max="31" default="1" size="20" /> 
	Tháng: <input type="number" name="thangdi" min="1" max="12" default="1" size="20" />
	Năm: <input type="number" name="namdi" min="2015" default="2015" size="20" />
	Giờ: <input type="number" name="giodi" min="0" max="24" default="1"  size="20" />
	Phút: <input type="number" name="phutdi" min="0" max="60" default="0" size="20" /><br />*<?php echo $thoigiandiErr;?></span><br />
	<p>Thời gian đến: </p><br />
	Ngày: <input type="number" name="ngayden" min="1" max="31" default="1"  size="20" /> 
	Tháng: <input type="number" name="thangden" min="1" max="12" default="1"  size="20" />
	Năm: <input type="number" name="namden" min="2015" default="2015" size="20" />
	Giờ: <input type="number" name="gioden" min="0" max="24" default="1" size="20" />
	Phút: <input type="number" name="phutden" min="0" max="60" default="0" size="20" /><br />*<?php echo $thoigiandenErr;?></span><br />
	<input type="submit" name="addchuyenbay" value="Thêm" />
</form>

<?php
	if ($validated == 1) {
$sql1=pg_query("INSERT INTO chuyenbay(chuyenbayid, diemroi, diemden, cuaden, hangbayid, maybay, soghetoida, soghedadat, thoigiandi, thoigianden) 
	VALUES ('".$chuyenbayid."', "."'".$diemroi."', "."'".$diemden."', "."'".$cuaden."', '".$hangbayid."', "."'".$maybay."', ".$soghetoida." :: int, ".$soghedadat." :: int, make_timestamp(".$namdi.", ".$thangdi.", ".$ngaydi.", ".$giodi.", ".$phutdi.", 00 :: double precision), make_timestamp(".$namden.", ".$thangden.", ".$ngayden.", ".$gioden.", ".$phutden.", 00 :: double precision))");
$sql=pg_query("SELECT * FROM chuyenbay WHERE chuyenbayid = '".$chuyenbayid."'");
	}
?>
<table id='display'>

		
 <div class="example">
        <div class="container">
            <div class="row">
                <h2>Table Basic</h2>
                <table class="table table-bordered">
			<thread>
				<tr>
       				 <td><b>Chuyến bay ID</td>
     				 <td><b>Điểm rời</td>
     				 <td><b>Điểm đến</td>
      				 <td><b>Cửa đến</td>
      				 <td><b>Cửa đi</td>
     				 <td><b>Hãng bay ID</td>
     				 <td><b>Máy bay</td>
      				 <td><b>Còn chỗ</td>
      				 <td><b>Số ghế tối đa</td>
     				 <td><b>Số ghế đã đặt</td>
     				 <td><b>Thời gian đi</td>
      				 <td><b>Thời gian đến</td>
        			 </tr>
			</thread>
<tbody>
    <?php
        while($rows = pg_fetch_array($sql))
            {
    ?>
        <tr>
        	<td><b><?php echo $rows['chuyenbayid'] ?></td>
        	<td><b><?php echo $rows['diemroi'] ?></td>
			<td><b><?php echo $rows['diemden'] ?></td>
			<td><b><?php echo $rows['cuaden'] ?></td>
			<td><b><?php echo $rows['cuadi'] ?></td>
        	<td><b><?php echo $rows['hangbayid'] ?></td>
			<td><b><?php echo $rows['maybay'] ?></td>
			<td><b><?php echo $rows['concho'] ?></td>
			<td><b><?php echo $rows['soghetoida'] ?></td>
        	<td><b><?php echo $rows['soghedadat'] ?></td>
			<td><b><?php echo $rows['thoigiandi'] ?></td>
			<td><b><?php echo $rows['thoigianden'] ?></td>
		</tr>
        <?php
            }
        ?>
</tbody>
</table>
</div>
</div>
</div>

</body>
</html>