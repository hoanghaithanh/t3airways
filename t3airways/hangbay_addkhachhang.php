<?php 
session_start();
require_once('Connections/Myconnection.php');
if (($_SESSION['logged-in']!=true)) {
  header("Location: Index.php");
}
require_once('admin_hangbay_tool.php');
?>
	<?php
	$hotenErr = $cmndErr = $diachiErr = "";
	$name = $hanhkhachid = $cmnd = $ngaysinh = $diachi = "";
	$validated = 0;
	if ($_SERVER["REQUEST_METHOD"]=="POST") {
		$validated = 1;
		if (empty($_POST['hoten'])) {
			$hotenErr = "Bạn chưa nhập họ tên!";
			$validated = 0;
		} else {
			$hoten = test_input($_POST['hoten']);
		}

		if (empty($_POST['cmnd']) or (strlen($_POST['cmnd'])!=12)) {
			$cmndErr = "CMND không hợp lệ";
			$validated = 0;
		} else {
			$checkcmnd = pg_query("select * from hanhkhach where cmnd = '".$_POST['cmnd']."'");
			if(pg_num_rows($checkcmnd)>=1)
			{
				$cmndErr = "CMND không hợp lệ hoặc đã tồn tại hành khách trong dữ liệu!";
				$validated = 0;
			} else	$cmnd = test_input($_POST['cmnd']);
		}

		if (empty($_POST['ngay']) or empty($_POST['thang']) or empty($_POST['nam'])) {
			$ngaysinhErr = "Bạn chưa nhập đủ ngày sinh!";
			$validated = 0;
		} elseif (checkdate($_POST['thang'], $_POST['ngay'], $_POST['nam']) == FALSE ) {
			$ngaysinhErr = "Ngày sinh không hợp lệ!";
			$validated = 0;
		}
		else {
			$ngaysinh = $_POST['nam']."-".$_POST['thang']."-".$_POST['ngay'];
		}

		if (empty($_POST['diachi'])) {
			$diachiErr = "Bạn chưa nhập địa chỉ!";
			$validated = 0;
		} else {
			$diachi = test_input($_POST['diachi']);
		}
	}

	function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
	?>
<form action="hangbay_addkhachhang.php" method="post" name="form1" id="form1">
<div style="border:#F00 solid 1px; width:300px; margin:auto">
<div style="background:#F00; color:#FFF; text-align:center; padding: 5px 0px 5px 0px"><strong>Thêm Mới Khách Hàng</strong></div>
        <table width="255" align="center">
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Họ Tên :</td>
            <td><input type="text" name="hoten" value="" size="20" /><span class="error"><?php echo "<br />".$hotenErr;?></span></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">CMND:</td>
            <td><input type="text" name="cmnd" value="" size="20" /><span class="error"><?php echo "<br />".$cmndErr;?></span></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Địa Chỉ:</td>
            <td><input type="text" name="diachi" value="" size="20" /><span class="error"><?php echo "<br />".$diachiErr;?></span></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Ngày sinh:</td>
            <td><input type="text" name="ngay" value="" size="20" />
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Tháng sinh:</td>
            <td><input type="text" name="thang" value="" size="20" />
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Năm sinh:</td>
            <td><input type="text" name="nam" value="" size="20" /><span class="error"><?php echo "<br />".$ngaysinhErr;?></span></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">&nbsp;</td>
            <td><input name="submit" type="submit" value="Thêm Khách Hàng" /></td>
          </tr>
        </table>
</div>
</form>

<?php
	if ($validated == 1) {
		
$sql1=pg_query("INSERT INTO hanhkhach VALUES (add_id_hanhkhach(), '".$hoten."'".", '".$ngaysinh."'".", '".$cmnd."'".", '".$diachi."')");
$sql=pg_query("SELECT * FROM hanhkhach WHERE hanhkhachid = (select max(hanhkhachid) from hanhkhach)");
		$validated = 0;
?>
<table class="tablebg" border="0" width="800" align="center" cellpadding="1" cellspacing="1">
  <tr>
    <th width="80" rowspan="1" align="center">Khách hàng ID</th>
    <th width="220" rowspan="1" align="center">Họ tên</th>
    <th width="220" rowspan="1" align="center">Ngày sinh</th>
    <th width="90" rowspan="1" align="center">CMND</th>
    <th width="90" rowspan="1" align="center">Địa chỉ</th>
  </tr>
  <?php while ($row_RCdanh_sach = pg_fetch_assoc($sql)){ ?>
    <tr class="row">
      <td  class="row1" align="left"><?php echo $row_RCdanh_sach['hanhkhachid']; ?></td>
      <td  class="row1" align="left"><?php echo $row_RCdanh_sach['hoten']; ?></td>
      <td  class="row1" align="left"><?php echo $row_RCdanh_sach['ngaysinh']; ?></td>
      <td  class="row1" align="left"><?php echo $row_RCdanh_sach['cmnd']; ?></td>
      <td  class="row1" align="left"><?php echo $row_RCdanh_sach['diachi']; ?></td>
    </tr>
    <?php }  ?>
</table>
<?php
}
?>
</body>
</html>