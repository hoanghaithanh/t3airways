<?php 
require_once('Connections/Myconnection.php');
session_start();
require_once('admin_hangbay_tool.php'); 
if (($_SESSION['logged-in']!=true)) {
  header("Location: Index.php");

}
?>
  <?php
    $validated = 0;
    $hangbayid = $_SESSION['user_name'];
    $hoten = $ngay = $thang = $nam = $ngaysinh = "";
    $hotenErr = $ngaysinhErr = "";
    $submit = $_POST['submit'];

    if($submit!="")
    {
      $validated = 1;
    
      if(empty($_POST['hoten'])) 
      {
        $hotenErr = "Chưa nhập họ tên";
        $validated = 0;
      }
      else $hoten = $_POST['hoten'];

      if(empty($_POST['ngay']) or empty($_POST['thang']) or empty($_POST['nam']))
      {
        $ngaysinhErr = "Chưa nhập đủ ngày tháng năm sinh";
        $validated = 0;
      } 
      elseif(checkdate($_POST['thang'], $_POST['ngay'], $_POST['nam']) == FALSE)
      {
        $validated = 0;
        $ngaysinhErr = "Ngày sinh không hợp lệ";
      } 
      else
      {
      $ngaysinh = $_POST['nam']."-".$_POST['thang']."-".$_POST['ngay'];
      }
      
      if($validated == 1)
      {
        $themnhanvien = pg_query("INSERT INTO nhanvien VALUES (add_id_nhanvien(),'".$hoten."', '".$ngaysinh."', '".$hangbayid."')");
          
          if($themnhanvien)
          {
            echo "<center>Thêm nhân viên thành công!</center>";
          }
          else echo "<center>Thêm nhân viên thất bại!</center>";
      }
    } 
  ?>
    
  <form action="addnhanvien.php" method="post" name="form1" id="form1">
<div style="border:#F00 solid 1px; width:250px; margin:auto">
<div style="background:#F00; color:#FFF; text-align:center; padding: 5px 0px 5px 0px"><strong>Thêm nhân viên</strong></div>
        <table width="200" align="center">
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Họ Tên:</td>
            <td><input type="text" name="hoten" value="" size="20" /><span class="error"><?php echo "<br />".$hotenErr;?></span></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Ngày sinh:</td>
            <td><input type="text" name="ngay" value="" size="20" /></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Tháng sinh:</td>
            <td><input type="text" name="thang" value="" size="20" /></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Năm sinh:</td>
            <td><input type="text" name="nam" value="" size="20" /><span class="error"><?php echo "<br />".$ngaysinhErr;?></span></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">&nbsp;</td>
            <td><input name="submit" type="submit" value="Thêm" /></td>
          </tr>
        </table>
</div>
</form>
<?php
  if($themnhanvien)
  {
?>  
<?php
    $sql=pg_query("select * from nhanvien where nhanvienid = (select max(nhanvienid) from nhanvien)");
    ?>
<table class="tablebg" border="0" width="800" align="center" cellpadding="1" cellspacing="1">
  <tr>
    <th width="80" rowspan="1" align="center">Nhân viên ID</th>
    <th width="220" rowspan="1" align="center">Họ tên</th>
    <th width="90" rowspan="1" align="center">Ngày sinh</th>
    <th width="90" rowspan="1" align="center">Hãng bay ID</th>
  </tr>
  <?php while ($row_RCdanh_sach = pg_fetch_assoc($sql)){ ?>
    <tr class="row">
      <td  class="row1" align="left"><?php echo $row_RCdanh_sach['nhanvienid']; ?></td>
      <td  class="row1" align="left"><?php echo $row_RCdanh_sach['hoten']; ?></td>
      <td  class="row1" align="left"><?php echo $row_RCdanh_sach['ngaysinh']; ?></td>
      <td  class="row1" align="left"><?php echo $row_RCdanh_sach['hangbayid']; ?></td>
    </tr>
    <?php }  ?>
</table>
<?php
}
?>
</body>
</html>