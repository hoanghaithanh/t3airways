<?php
date_default_timezone_set("Asia/Bangkok");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="js/superfish/css/superfish.css" media="screen">
<script type="text/javascript" src="js/jquery-1.2.6.min.js"></script>
<script type="text/javascript" src="js/superfish/js/hoverIntent.js"></script>
<script type="text/javascript" src="js/superfish/js/superfish.js"></script>
<link href="css/main.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
  jQuery(function(){
    jQuery('ul.sf-menu').superfish();
  });
</script>
<title>Untitled Document</title>
<style type="text/css">
<!--
body,td,th {
  font-family: Arial, Helvetica, sans-serif;
  font-size: 12px;
  line-height: 1.4;
}
-->
</style>
</head>
<body>

<body>
<div id="topbar">
<div class="wrapper">
<ul class="sf-menu">
  <li class="current">
    <a href="#a">Hệ thống</a>
    <ul>
        <li><a href="logout.php">Đăng xuất</a></li>
        <li><a href="changepassword.php">Đổi mật khẩu </a></li>
    </ul>
  </li>
      <li><a href="#">Nội bộ</a>
          <ul>
              <li><a href="addnhanvien.php">Thêm nhân viên</a></li>
              <li><a href="deletenhanvien.php">Xóa nhân viên</a></li>
              <li><a href="searchnhanvienhangbay.php">Kiểm tra thông tin nhân viên</a></li>
              <li><a href="updatenhanvien.php">Thay đổi thông tin nhân viên</a></li>
            </ul>
        </li>
      <li><a href="#">Chuyến bay</a>
        <ul>
                <li><a href="hangbay_timkiemchuyenbay.php">Kiểm tra thông tin chuyến bay</a></li>
                <li><a href="danhsachhanhkhach.php">Danh sách hành khách trên chuyến bay</a></li>
                <li><a href="updatechuyenbay.php">Thay đổi thông tin chuyến bay</a></li>
                <li><a href="addchuyenbayden.php">Thêm chuyến bay đến</a></li>
                <li><a href="addchuyenbaydi.php">Thêm chuyến bay đi</a></li>
                <li><a href="deletechuyenbay.php">Xóa chuyến bay</a></li>
                <li><a href="thongkechuyenbay.php">Thống kê</a></li>
            </ul>
        </li>
        <li><a href="#">Khách hàng</a>
        <ul>
                <li><a href="timkiemkhachhang.php">Kiểm tra thông tin khách hàng</a></li>
                <li><a href="hangbay_addkhachhang.php">Thêm khách hàng</a></li>
                <li><a href="addchitietdatve.php">Thêm chi tiết đặt vé</a></li>
                <li><a href="deletechitietdatve.php">Xóa chi tiết đặt vé</a></li>
                <li><a href="kiemtrachitietdatve.php">Tìm kiếm thông tin vé</a></li>
            </ul>
        </li>
     
</ul>
<span class="hello"><strong><?php echo $_SESSION['user_name'] . ", " .Date("l F d, Y"); ?></strong></span>
</div>
</div>
<div class="top_space"></div>
<div class="wrapper">
<table align="center" width="980" border="0" cellspacing="0" cellpadding="0">
  <tr><td colspan="2"><img width="978" src="images/banner.jpg" /></td></tr>
  <tr>
    <td class="row4" width="170" valign="top"><table width="100%" border="0" cellspacing="1" cellpadding="10">
      <tr>
        <td class="row3"><a href="index.php">Trang chủ</a></td>
      </tr>
      
    </table></td>
    </tr>
</table>
</div>