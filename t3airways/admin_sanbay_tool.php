<?php
date_default_timezone_set("Asia/Bangkok");
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="js/superfish/css/superfish.css" media="screen">
<script type="text/javascript" src="js/jquery-1.2.6.min.js"></script>
<script type="text/javascript" src="js/superfish/js/hoverIntent.js"></script>
<script type="text/javascript" src="js/superfish/js/superfish.js"></script>
<link href="css/main.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
  jQuery(function(){
    jQuery('ul.sf-menu').superfish();
  });
</script>
<title>Untitled Document</title>
<style type="text/css">
<!--
body,td,th {
  font-family: Arial, Helvetica, sans-serif;
  font-size: 12px;
  line-height: 1.4;
}
-->
</style>
</head>
<body>
	<div id="topbar">
<div class="wrapper">
<ul class="sf-menu">
	<li class="current">
		<a href="#a">Hệ thống</a>
		<ul>
        <li><a href="logout.php">Đăng xuất</a></li>
        <li><a href="changepassword.php">Đổi mật khẩu </a></li>
		</ul>
	</li>
    	<li><a href="#">Hãng bay</a>
        	<ul>
            	<li><a href="addhangbay.php">Thêm hãng bay</a></li>
                <li><a href="deletehangbay.php">Xóa hãng bay</a></li>
                <li><a href="searchhangbay.php">Kiểm tra thông tin hãng bay</a></li>
            </ul>
        </li>
    	<li><a href="#">Nhân viên</a>
    		<ul>
                <li><a href="searchnhanvien.php">Kiểm tra thông tin nhân viên</a></li>
            </ul>
        </li>
        <li><a href="#">Khách hàng</a>
    		<ul>
                <li><a href="searchhanhkhach.php">Kiểm tra thông tin khách hàng</a></li>
                <li><a href="addhanhkhach.php">Thêm mới khách hàng</a></li>
                <li><a href="updatehanhkhach.php">Sửa thông tin khách hàng</a></li>
                <li><a href="deletehanhkhach.php">Xóa khách hàng</a></li>
            </ul>
        </li>
        <li><a href="#">Chuyến bay</a>
        <ul>
                <li><a href="timkiemchuyenbay.php">Kiểm tra thông tin chuyến bay</a></li>
                <li><a href="admin_danhsachhanhkhach.php">Danh sách hành khách trên chuyến bay</a></li>
                <li><a href="listhacanh.php">Danh sách các chuyến bay sắp hạ cánh</a></li>
                <li><a href="listcatcanh.php">Danh sách các chuyến bay sắp cất cánh</a></li>
            </ul>
        </li>
     
</ul>
<span class="hello"><strong><?php echo $_SESSION['user_name'] . ", " .Date("l F d, Y"); ?></strong></span>
</div>
</div>
<div class="top_space"></div>
<div class="wrapper">
<table align="center" width="980" border="0" cellspacing="0" cellpadding="0">
  <tr><td colspan="2"><img width="978" src="images/banner.jpg" /></td></tr>
  <tr>
    <td class="row4" width="170" valign="top"><table width="100%" border="0" cellspacing="1" cellpadding="10">
      <tr>
        <td class="row3"><a href="index.php">Trang chủ</a></td>
      </tr>
</table>
</table>
</div>