<?php 
require_once('Connections/Myconnection.php');
session_start(); 
require_once('admin_hangbay_tool.php');
if (($_SESSION['logged-in']!=true)) {
  header("Location: Index.php");

}
?>
  <?php
  $sure = $_POST['sure'];
  if ($_POST['submit1']=="Xóa"){
     $validated = 1;
    if(empty($_POST['chuyenbayid'])){
      $chuyenbayidErr = "Chưa nhập chuyến bay ID";
      $validated = 0;
    }
     else {
      $sql=pg_query("select * from chuyenbay where chuyenbayid = '".$_POST['chuyenbayid']."' and hangbayid = '".$_SESSION['user_name']."'");
      if(pg_num_rows($sql)<1)
      {
        $validated = 0;
        $chuyenbayidErr = "Không tồn tại ID đã nhập hoặc chuyến bay không do hãng bay bạn quản lý";
      }
      else {
        $row = pg_fetch_assoc($sql);
        $thoigiandi = $row['thoigiandi'];
        $time_now = date("Y-m-d H:i:s");
        if ($time_now > $thoigiandi)
        {
          $validated = 0;
          $chuyenbayidErr = "Chuyến bay này đã cất cánh!";
        }
        else $chuyenbayid = $_POST['chuyenbayid'];
      }
    }


     if(empty($_POST['veid'])){
      $veidErr = "Chưa nhập Vé ID";
      $validated = 0;
    }
     else {
      $sql=pg_query("select * from chitietdatve where veid = '".$_POST['veid']."' and chuyenbayid = '".$chuyenbayid."'");
      if(pg_num_rows($sql)>=1)
      {
        $row1 = pg_fetch_assoc($sql);
        $veid = $_POST['veid'];
        $hanhkhachid = $row1['hanhkhachid'];
        $hangbayid = $row1['hangbayid'];
        $hangthuonggia = $row1['hangthuonggia'];
      }
      else 
      {
        $validated = 0;
        $veidErr = "Không tồn tại ID này hoặc chuyến bay không thuộc hãng bay bạn quản lý!";
      }
     }
  }
  ?>


  <?php if($validated != 1)
  {
    ?>
<form action="deletechitietdatve.php" method="post" name="form1" id="form1">
<div style="border:#F00 solid 1px; width:300px; margin:auto">
<div style="background:#F00; color:#FFF; text-align:center; padding: 5px 0px 5px 0px"><strong>Xóa Chi Tiết Đặt Vé</strong></div>
        <table width="255" align="center">
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Chuyến bay ID:</td>
            <td><input type="text" name="chuyenbayid" value="" size="24" /><span class="error"><?php echo "<br />".$chuyenbayidErr;?></span></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Vé ID:</td>
            <td><input type="text" name="veid" value="" size="24" /><span class="error"><?php echo "<br />".$veidErr;?></span></td>
          </tr>
             <tr valign="baseline">
            <td nowrap="nowrap" align="right">&nbsp;</td>
            <td><input name="submit1" type="submit" value="Xóa" /></td>
          </tr>
        </table>
</div>
</form>

<?php
}
?>

<?php
if($validated == 1 and $sure !=1)
{
?>
<form action="deletechitietdatve.php" method="post" name="form1" id="form1">
<div style="border:#F00 solid 1px; width:400px; margin:auto">
<div style="background:#F00; color:#FFF; text-align:center; padding: 5px 0px 5px 0px"><strong>Xóa Chi Tiết Đặt Vé</strong></div>
        <table width="255" align="center">
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Chuyến bay ID:</td>
            <td><input type="text" name="chuyenbayid" value=<?php echo "\"".$chuyenbayid."\""?> size="24" readonly/></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Vé ID:</td>
            <td><input type="text" name="veid" value=<?php echo "\"".$veid."\""?> size="24" readonly/></td>
          </tr>
            <tr valign="baseline">
            <td nowrap="nowrap" align="right">Hành khách ID:</td>
            <td><input type="text" name="hanhkhachid" value=<?php echo "\"".$hanhkhachid."\""?> size="24" readonly/></td>
          </tr>
          <tr valign="baseline">
            <td nowrap="nowrap" align="right">Hạng thương gia:</td>
            <td><input type="text" name="hangthuonggia" value=<?php echo "\"".$hangthuonggia."\""?> size="24" readonly/></td>
          </tr>
          <tr valign="baseline">
              <td nowrap="nowrap" align="right">Bạn có chắc chắn muốn xóa không:</td>
            </tr>
            <tr valign="baseline">
              <td nowrap="nowrap" align="right">Có:</td>
            <td><input type="checkbox" name="sure" value="1" /><span class="error"></td>
            </tr>
            
             <tr valign="baseline">
            <td nowrap="nowrap" align="right">&nbsp;</td>
            <td><input name="submit2" type="submit" value="Xóa" /></td>
          </tr>
        </table>
</div>
</form>
<?php
}
?>

<?php
  if($sure == 1) {
        $sql=pg_query("select * from chitietdatve where veid = '".$_POST['veid']."' and chuyenbayid = '".$_POST['chuyenbayid']."'");
        $sql1=pg_query("DELETE from chitietdatve where chuyenbayid = '".$_POST['chuyenbayid']."' AND veid = '".$_POST['veid']."'");
        if($sql1)
        {
          echo "<center>Xóa chi tiết đặt vé thành công!</center>";
        }
        ?>
<table class="tablebg" border="0" width="800" align="center" cellpadding="1" cellspacing="1">
  <tr>
    <th width="80" rowspan="1" align="center">Chuyến bay ID</th>
    <th width="220" rowspan="1" align="center">Vé ID</th>
    <th width="90" rowspan="1" align="center">Hành khách ID</th>
    <th width="90" rowspan="1" align="center">Hạng thương gia</th>
  </tr>
  <?php while ($row_RCdanh_sach = pg_fetch_assoc($sql)){ ?>
    <tr class="row">
      <td  class="row1" align="center"><?php echo $row_RCdanh_sach['chuyenbayid']; ?></td>
      <td  class="row1" align="center"><?php echo $row_RCdanh_sach['veid']; ?></td>
      <td  class="row1" align="center"><?php echo $row_RCdanh_sach['hanhkhachid']; ?></td>
      <td  class="row1" align="center"><?php echo $row_RCdanh_sach['hangthuonggia']; ?></td>
    </tr>
    <?php }  ?>
</table>
<?php
}
?>
</body>
</html>